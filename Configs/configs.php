<?php

return [
	'envio' => [
		'nome-completo' => 'Cleberson Falk',
		'email' => 'cleberson.falk@gmail.com',
		'mensagem' => 'Olá, em busca de uma nova oportunidade, apresento meu currículo para sua apreciação. Caso tenha necessidades de mais informações, favor entrar em contato para mais esclarecimentos. 
Desde já agradeço a atenção prestada, e fico no aguardo do seu contato, caso o meu perfil se enquadre na vaga.',
		'envia-curriculo[]' => '',
		'wp_job_manager_send_application' => '1',
		'job_id' => ''
	]
];