<?php

/**
 * JobsFinder - Sistema de busca de vagas e envio de currículos via terminal
 * Copyright (C) 2017, Cleberson Falk.
 *
 * This file is part of Jobs Finder.
 *
 * JobsFinder is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JobsFinder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JobsFinder.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Cleberson Falk <cleberson.falk@gmail.com>
 * @copyright 2017 Free Software Foundation, Inc http://www.fsf.org
 * @license  GNU General Public License http://www.gnu.org/licenses/
 * @version  0.1.0
 * @link
 */

namespace Clebersonfalk\Jobsfinder\Db;

class Database
{
	private static $instance = null;

	public static function getInstance()
	{
		if(!isset(self::$instance)) {
			self::$instance = new \PDO('sqlite:' . APP_PATH . '/../Database/database.db');
		}

		return self::$instance;
	}
}