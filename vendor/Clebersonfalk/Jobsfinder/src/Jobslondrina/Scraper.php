<?php

/**
 * JobsFinder - Sistema de busca de vagas e envio de currículos via terminal
 * Copyright (C) 2017, Cleberson Falk.
 *
 * This file is part of Jobs Finder.
 *
 * JobsFinder is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JobsFinder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JobsFinder.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Cleberson Falk <cleberson.falk@gmail.com>
 * @copyright 2017 Free Software Foundation, Inc http://www.fsf.org
 * @license  GNU General Public License http://www.gnu.org/licenses/
 * @version  0.1.0
 * @link
 */

namespace Clebersonfalk\Jobsfinder\Jobslondrina;

use Clebersonfalk\Jobsfinder\Db\Database;

/**
 * A Classe Scraper é responsável por fazer a requisição
 * com o site jobslondrina.com e extrair as informações
 * sobre as vagas de emprego.
 *
 * @package Clebersonfalk\Jobsfinder\Jobslondrina
 */
class Scraper
{
	protected $url = "";
	protected $html = "";
	protected $data = array();

	/**
	 * Scraper constructor.
	 *
	 * @param $url
	 */
	public function __construct($url)
	{
		$this->url = $url;
	}

	/**
	 * Método responsável por enviar a requisição POST e
	 * receber os dados de retorno.
	 *
	 * @return mixed
	 */
	public function loadHtml($fields = array())
	{
		$ch = curl_init($this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
			'Origin' => 'http://jobslondrina.com'
		));
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->parseFields($fields));
		$html = curl_exec($ch);
		curl_close($ch);

		$this->html = $html;

		return $this;
	}

	/**
	 * Método que monta a url da requisição GET
	 *
	 * @param $fields
	 * @return string
	 */
	protected function parseFields($fields)
	{
		$query_string = '?';

		foreach($fields as $key => $field) {
			$query_string .= $field . '&';
		}

		rtrim($query_string, "&");

		return $query_string;
	}

	/**
	 * Método que extrai as informações das vagas do XML
	 *
	 * @return $this
	 */
	public function scrapData()
	{
		$json 	= json_decode($this->html);
		$xml 	= simplexml_load_string('<xml>' . $json->html . '</xml>');

		foreach($xml as $item) {
			// Extrai o ID da vaga das classes na li
			$classes = $item->attributes()->__toString();
			$class_array = preg_grep("/post-(\d+)/i", explode(' ', $classes));
			$id = end(explode('-', current($class_array)));

			$temp = array(
				'id' 		=> $id,
				'url' 		=> $item->a->attributes()->href->__toString(),
				'tipo_vaga' => $item->a->children()->ul->children()->li[0]->__toString(),
				'titulo' 	=> $item->a->children()->div->children()->h3->__toString(),
				'empresa' 	=> $item->a->children()->div->children()->div->children()->strong->__toString(),
				'data' 		=> $item->a->children()->ul->children()->li[1]->date->__toString()
			);
			$this->data[] = $temp;
		}

		return $this;
	}

	/**
	 * Método que lista as vagas no terminal
	 */
	private function printData()
	{
		$vagas = $this->data;
		$header = array(
			'id' => '',
			'url' => '',
			'tipo_vaga' => 'Tipo de vaga',
			'titulo' => 'Descrição da vaga',
			'empresa' => 'Empresa',
			'data' => 'Data');

		array_unshift($vagas, $header);
		$keys = $this->getHighestField();

		foreach($vagas as $key => $vaga) {
			$diff_tipo = strlen($vaga['tipo_vaga']) - mb_strlen($vaga['tipo_vaga']);
			$diff_titulo = strlen($vaga['titulo']) - mb_strlen($vaga['titulo']);
			$diff_empresa = strlen($vaga['empresa']) - mb_strlen($vaga['empresa']);
			$diff_data = strlen($vaga['data']) - mb_strlen($vaga['data']);

			if($key == 0) {
				$row = "\033[34m";
				$row .= str_pad($vaga['tipo_vaga'], ($keys['tipo_vaga']+15), " ", STR_PAD_RIGHT) . " | ";
			}
			else {
				$row = "\033[31mJobs Londrina: " . "\033[32m";
				$row .= str_pad($vaga['tipo_vaga'], ($keys['tipo_vaga'] + $diff_tipo), " ", STR_PAD_RIGHT) . " | ";
			}

			$row .= str_pad($vaga['titulo'], 	($keys['titulo'] + $diff_titulo), " ", STR_PAD_RIGHT) 	. " | ";
			$row .= str_pad($vaga['empresa'], 	($keys['empresa'] + $diff_empresa), " ", STR_PAD_RIGHT) 	. " | ";
			$row .= str_pad($vaga['data'], 		($keys['data'] + $diff_data), " ", STR_PAD_RIGHT) 		. "\n";

			print $row;
		}
	}

	/**
	 * Método que verifica qual a maior texto de cada coluna
	 * para o alinhamento.
	 *
	 * @return array
	 */
	private function getHighestField()
	{
		$keys = array_fill_keys(array_keys($this->data[0]), 0);

		foreach($this->data as $key => $item) {
			foreach($keys as $k => $v) {
				if(strlen($item[$k]) > $keys[$k]) {
					$keys[$k] = strlen($item[$k]);
				}
			}
		}

		return $keys;
	}

	/**
	 * Salva as vagas no Banco de Dados
	 *
	 * @return $this
	 */
	public function save()
	{
		$db = Database::getInstance();

		try {
			foreach($this->data as $vaga) {
				// Verifica se a vaga já está cadastrada
				$query = "SELECT idvaga FROM vagas WHERE url = '" . $vaga['url'] . "'";
				$stmt = $db->query($query);
				$result = $stmt->fetch(\PDO::FETCH_NUM);

				if(!$result) {
					$stmt = $db->prepare("INSERT INTO vagas(id,url,tipo_vaga,titulo,empresa,data,enviado)
						VALUES(:id,:url,:tipo_vaga,:titulo,:empresa,:data, 0);");
					$result = $stmt->execute($vaga);
				}
			}
		}
		catch (\Exception $e) {
			echo 'Erro ao inserir vagas. ' . $e->getMessage();
		}

		$this->printData();

		return $this;
	}

	/**
	 * Método responsável pelo envio do currículo
	 */
	public function send($config)
	{
		$db = Database::getInstance();

		$stmt = $db->query('SELECT * FROM vagas WHERE enviado=0');
		$vagas = $stmt->fetchAll();

		$enviados = 0;
		foreach($vagas as $key => $vaga) {
			// Atualiza as vagas que já foram enviadas
			$stmt = $db->prepare("UPDATE vagas SET enviado=1 WHERE url=:url");
			$stmt->execute(array('url' => $vaga['url']));

			$file = APP_PATH . '/../Data/Currículo.pdf';
			if(function_exists('curl_file_create')) {
				$cFile = curl_file_create($file);
			}
			else {
				$cFile = '@' . realpath($file);
			}

			$fields = $config['envio'];
			$fields['envia-curriculo[]'] = $cFile;
			$fields['job_id'] = $vaga['id'];

//			var_dump($vaga['url'], $fields);

//			$ch = curl_init($vaga['url']);
//			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//			curl_setopt($ch, CURLOPT_POST, count($fields));
//			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
//			$result = curl_exec($ch);
//			curl_close($ch);

			$enviados++;
		}

		$keys = $this->getHighestField();
		print "\033[0;37m". str_repeat('_', (array_sum($keys)) - 70) ."\n";
		print "\033[0m{$enviados} currículos enviados.\n";
	}
}