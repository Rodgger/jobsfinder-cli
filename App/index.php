<?php

/**
 * JobsFinder - Sistema de busca de vagas e envio de currículos via terminal
 * Copyright (C) 2017, Cleberson Falk.
 *
 * This file is part of Jobs Finder.
 *
 * JobsFinder is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JobsFinder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JobsFinder.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Cleberson Falk <cleberson.falk@gmail.com>
 * @copyright 2017 Free Software Foundation, Inc http://www.fsf.org
 * @license  GNU General Public License http://www.gnu.org/licenses/
 * @version  0.1.0
 * @link
 */

namespace App;

require __DIR__ . '/../vendor/autoload.php';
$config = include __DIR__ . '/../Configs/configs.php';

use Clebersonfalk\Jobsfinder\Jobslondrina\Scraper;

if(!defined('APP_PATH')) {
	define("APP_PATH", __DIR__);
}

//error_reporting(E_ALL);
//ini_set('display_errors', true);

$params = array(
	'search_keywords=',
	'search_location=',
	'search_categories[]=11',
	'filter_job_type[]=emprego',
//	'filter_job_type[]=estagio',
	'filter_job_type[]=freelancer',
	'filter_job_type[]=temporario',
	'per_page=15',
	'orderby=featured',
	'order=DESC',
	'page=1',
	'show_pagination=false'
);

$scraper = new Scraper("http://jobslondrina.com/jm-ajax/get_listings/");
$vagas = $scraper->loadHtml($params)
	->scrapData()
	->save()
	->send($config);

//$vagas = $scraper->loadHtml($params)
//	->scrapData();
